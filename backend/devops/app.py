from flask import Flask
from flask import jsonify


APP = Flask(__name__)


def getlist():
    return jsonify({
        'items': [
            'Wash a car',
            'Make a call to David',
            'Do a homework',
        ]
    })


def getdict():
    return jsonify({
        'Dan': '+7 905 484 39 32',
        'Victor': '+549 92 83 8239',
        'Barak': '+11 329 9238 29',
        'Niesah': '+89 8444 93 349',
    })


APP.add_url_rule('/getlist', 'getlist', getlist)
APP.add_url_rule('/getdict', 'getdict', getdict)
