import unittest


class SimpleTest(unittest.TestCase):
    def test_nothing(self):
        self.assertEqual(0, 0)


if __name__ == '__main__':
    unittest.main()
