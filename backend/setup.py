from setuptools import setup


setup(
    name='devops',
    package=['devops'],
    include_package_data=True,
)
