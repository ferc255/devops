import React, { Component } from 'react';
import Counter from './counter';


export default class Counters extends Component {
  render() {
    return (
      <div>
        <span>{this.props.items.map(item => (
          <Counter
            key={item.id}
            item={item}
          />
        ))}</span>
      </div>
    );
  }
}
