import React, { Component } from 'react';


export default class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-light bg-light">
        <span>
        <button
          className="btn btn-outline-success"
          type="button"
          onClick={this.getList}
        >
          TODO list
        </button>
        <button
          className="btn btn-outline-warning"
          type="button"
          onClick={this.getDict}
        >
          Contacts
        </button>
        </span>
      </nav>
    );
  }

  createListFromJson = data => {
    let items = null;
    if ('items' in data) {
      items = data['items'].map((elem, index) => {
        return {
          id: index,
          key: elem,
        }
      });
    } else {
      items = Object.keys(data).map((elem, index) => {
        return {
          id: index,
          key: elem,
          value: data[elem],
        }
      });
    }
    this.props.updateState(items);
  }

  getList = () => {
    fetch('/api/getlist').then(
      result => result.json()).then(
      result => this.createListFromJson(result));
  }

  getDict = () => {
    fetch('/api/getdict').then(
      result => result.json()).then(
      result => this.createListFromJson(result));
  }
}
