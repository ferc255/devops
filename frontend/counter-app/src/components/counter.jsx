import React, { Component } from 'react';

class Counter extends Component {
  render() {
    return (
      <div>
        {this.handleDisplaying()}
      </div>
    );
  }

  handleDisplaying = () => {
    let item = this.props.item;
    if ('value' in item) {
      return (
        <React.Fragment>
          <b>{item.key}</b> : {item.value}
        </React.Fragment>
      );
    } else {
      return <div>{item.key}</div>;
    }
  }
}

export default Counter;
