import React, { Component } from 'react';
import NavBar from './components/navbar';
import Counters from './components/counters';
import './App.css';

class App extends Component {
  state = {
    items: [],
  }
  
  render() {
    return (
      <React.Fragment>
        <NavBar
          updateState={this.updateState}
        />
        <Counters
          items={this.state.items}
        />
      </React.Fragment>
    );
  }

  updateState = items => {
    this.setState({ items });
  }
}

export default App;
